package gusl.security.roles;

import gusl.core.logging.GUSLLogManager;
import gusl.core.logging.GUSLLogger;
import gusl.node.bootstrap.GUSLServiceLocator;
import org.glassfish.jersey.server.internal.LocalizationMessages;
import org.glassfish.jersey.server.model.AnnotatedMethod;

import javax.annotation.Priority;
import javax.annotation.security.DenyAll;
import javax.annotation.security.PermitAll;
import javax.annotation.security.RolesAllowed;
import javax.ws.rs.ForbiddenException;
import javax.ws.rs.Priorities;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.container.DynamicFeature;
import javax.ws.rs.container.ResourceInfo;
import javax.ws.rs.core.FeatureContext;

import static java.util.Objects.isNull;

/**
 * RolesAllowed Dynamic Feature.
 * <p>
 * The Jersey one is, well, a bit poo.
 * <p>
 * It allows unauthed methods to be executed by default. What we want is that
 * unless a resource (method) has @PermitAll (class or method) or @RolesAllowed,
 * (class or method) it will be denied.
 * <p>
 * To use this security feature, register it with
 * register(LmRolesAllowedDynamicFeature.class); in the application.
 *
 * @author dhudson
 */
public class GUSLRolesAllowedDynamicFeature implements DynamicFeature {

    protected final static GUSLLogger logger = GUSLLogManager.getLogger(GUSLRolesAllowedDynamicFeature.class);

    @Override
    public void configure(final ResourceInfo resourceInfo, final FeatureContext configuration) {

        // First things first, is it one of ours?
        if (!GUSLServiceLocator.hasCorrectPrefix(resourceInfo.getResourceClass().getName())) {
            return;
        }

        final AnnotatedMethod am = new AnnotatedMethod(resourceInfo.getResourceMethod());

        // DenyAll on the method take precedence over RolesAllowed and PermitAll
        if (am.isAnnotationPresent(DenyAll.class)) {
            configuration.register(new RolesAllowedRequestFilter());
            return;
        }

        // RolesAllowed on the method takes precedence over PermitAll
        RolesAllowed ra = am.getAnnotation(RolesAllowed.class);
        if (ra != null) {
            configuration.register(new RolesAllowedRequestFilter(ra.value()));
            return;
        }

        // PermitAll takes precedence over RolesAllowed on the class
        if (am.isAnnotationPresent(PermitAll.class)) {
            // Do nothing.
            return;
        }

        // RolesAllowed on the class takes precedence over PermitAll
        ra = resourceInfo.getResourceClass().getAnnotation(RolesAllowed.class);
        if (ra != null) {
            configuration.register(new RolesAllowedRequestFilter(ra.value()));
            return;
        }

        if (resourceInfo.getResourceClass().getAnnotation(PermitAll.class) != null) {
            // PermitAll at class level
            return;
        }

        if (resourceInfo.getResourceClass().getAnnotation(DenyAll.class) != null) {
            configuration.register(new RolesAllowedRequestFilter());
            return;
        }

        logger.warn("Unannotated resource method, access will be denied. {}:{}", resourceInfo.getResourceClass().getName(), am.getMethod().getName());
        // Reject the resource as it doesn't have an annotation
        configuration.register(new RolesAllowedRequestFilter());
    }

    @Priority(Priorities.AUTHORIZATION) // authorization filter - should go after any authentication filters
    private static class RolesAllowedRequestFilter implements ContainerRequestFilter {

        private final boolean denyAll;
        private final String[] rolesAllowed;

        RolesAllowedRequestFilter() {
            this.denyAll = true;
            this.rolesAllowed = null;
        }

        RolesAllowedRequestFilter(final String[] rolesAllowed) {
            this.denyAll = false;
            this.rolesAllowed = (rolesAllowed != null) ? rolesAllowed : new String[]{};
        }

        @Override
        public void filter(final ContainerRequestContext requestContext) {
            if (!denyAll) {
                if (isNull(rolesAllowed) || rolesAllowed.length > 0 && !isAuthenticated(requestContext)) {
                    throw new ForbiddenException(LocalizationMessages.USER_NOT_AUTHORIZED());
                }

                for (final String role : rolesAllowed) {
                    if (requestContext.getSecurityContext().isUserInRole(role)) {
                        return;
                    }
                }
            }

            throw new ForbiddenException(LocalizationMessages.USER_NOT_AUTHORIZED());
        }

        private static boolean isAuthenticated(final ContainerRequestContext requestContext) {
            if (requestContext.getSecurityContext() == null) {
                return false;
            }
            return requestContext.getSecurityContext().getUserPrincipal() != null;
        }
    }
}
