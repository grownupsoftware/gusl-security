package gusl.security.roles;

import javax.ws.rs.core.SecurityContext;
import java.security.Principal;

public abstract class GUSLSecurityContext implements SecurityContext {

    private final Principal thePrinciple;

    public GUSLSecurityContext(String name) {
        thePrinciple = () -> name;
    }

    @Override
    public Principal getUserPrincipal() {
        return thePrinciple;
    }

    @Override
    public boolean isSecure() {
        return false;
    }

    @Override
    public String getAuthenticationScheme() {
        return null;
    }

}
