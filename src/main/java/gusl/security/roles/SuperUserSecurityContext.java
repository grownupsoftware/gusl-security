package gusl.security.roles;

/**
 * Super Users are cool, they can do anything.
 *
 * @author dhudson
 */
public class SuperUserSecurityContext extends GUSLSecurityContext {

    public SuperUserSecurityContext(String name) {
        super(name);
    }

    @Override
    public boolean isUserInRole(String role) {
        return true;
    }
}
