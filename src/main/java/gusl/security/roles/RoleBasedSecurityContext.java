package gusl.security.roles;

import java.util.List;

/**
 * So we create one of these per login.
 * <p>
 * This means that if you change the roles for the user, it will not take effect
 * until they re-login.
 *
 * @author dhudson
 */
public class RoleBasedSecurityContext extends GUSLSecurityContext {

    private final List<String> theRoles;

    public RoleBasedSecurityContext(String name, List<String> roles) {
        super(name);
        theRoles = roles;
    }

    @Override
    public boolean isUserInRole(String role) {
        return theRoles.contains(role.toUpperCase());
    }
}
