package gusl.security.roles;

import gusl.model.exceptions.ValidationException;
import org.jvnet.hk2.annotations.Contract;

import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.core.SecurityContext;

/**
 * Provide a Security context for a request.
 *
 * @author dhudson
 */
@Contract
public interface RoleSecurityContextProvider {

    SecurityContext getSecurityContextFor(ContainerRequestContext requestContext) throws ValidationException;

}
