/* Copyright lottomart */
package gusl.security.roles;

import gusl.core.logging.GUSLLogManager;
import gusl.core.logging.GUSLLogger;
import gusl.model.exceptions.ValidationException;

import javax.annotation.Priority;
import javax.inject.Inject;
import javax.ws.rs.Priorities;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.Provider;
import java.io.IOException;
//import static gusl.node.security.RoleSecurityContextProvider.TOKEN_HEADER_NAME;

/**
 * Create a Filter which will set the security context for each request.
 * <p>
 * This allows for the javax.annotation.security annotations
 *
 * @author dhudson
 */
@Priority(Priorities.AUTHENTICATION)
@Provider
public class RolesSecurityFilter implements ContainerRequestFilter {

    protected final static GUSLLogger logger = GUSLLogManager.getLogger(RolesSecurityFilter.class);

    @Inject
    private RoleSecurityContextProvider theSecurityService;

    public RolesSecurityFilter() {
    }

    @Override
    public void filter(ContainerRequestContext requestContext) throws IOException {

        if (requestContext.getMethod().equals("OPTIONS")) {
            return;
        }

//        String token = requestContext.getHeaderString(TOKEN_HEADER_NAME);
//        // if token == null, then there is no security context.
//        if (token == null) {
//            // This is probably one of our internal calls
//            logger.debug("No security context for request {} ", requestContext.getUriInfo().getPath());
//            requestContext.setSecurityContext(null);
//            return;
//        }

        try {
            requestContext.setSecurityContext(theSecurityService.getSecurityContextFor(requestContext));
        } catch (ValidationException ex) {
            logger.warn("Rejecting request {}", requestContext.getUriInfo().getPath(), ex);
            // We don't like the token, so lets just respond
            requestContext.abortWith(Response.status(403)
                    .entity(ex)
                    .type(MediaType.APPLICATION_JSON)
                    .build());
        }
    }

}
